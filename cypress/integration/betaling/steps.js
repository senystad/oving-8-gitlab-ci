import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

// Registrer kjøpet
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Ola Nordmann').blur();
    cy.get('#address').type('Hakkebakkeskogen 1').blur();
    cy.get('#postCode').type('1234').blur();
    cy.get('#city').type('Skogen').blur();
    cy.get('#creditCardNo').type('1234567890123456').blur();
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('#submit').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, function () {
    cy.contains('Din ordre er registrert.');
});


// Feilmeldinger
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear().blur();
    cy.get('#address').clear().blur();
    cy.get('#postCode').clear().blur();
    cy.get('#city').type('Skogen').clear().blur();
    cy.get('#creditCardNo').clear().blur();
    cy.get('#submit').click();
});

Then(/^skal jeg få feilmeldinger for disse$/, function () {
    cy.contains('#fullNameError', 'Feltet må ha en verdi');
    cy.contains('#addressError', 'Feltet må ha en verdi');
    cy.contains('#postCodeError', 'Feltet må ha en verdi');
    cy.contains('#cityError', 'Feltet må ha en verdi');
    cy.contains('#creditCardNoError', 'Kredittkortnummeret må bestå av 16 siffer');
});